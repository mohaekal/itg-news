import axios from 'axios';
 
export function getPost(postId) {
    return axios.get('https://bundadaycare.com/itg/api/post/' + postId +'.json');
}
 
export function getComments(postId) {
    return axios.get(`https://bundadaycare.com/itg/api/post/${postId}/comments.json`)
}